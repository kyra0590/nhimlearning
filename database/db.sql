-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.14-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for nhim_learning
CREATE DATABASE IF NOT EXISTS `nhim_learning` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `nhim_learning`;

-- Dumping structure for table nhim_learning.cate_celebrate
CREATE TABLE IF NOT EXISTS `cate_celebrate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table nhim_learning.cate_celebrate: 0 rows
/*!40000 ALTER TABLE `cate_celebrate` DISABLE KEYS */;
/*!40000 ALTER TABLE `cate_celebrate` ENABLE KEYS */;

-- Dumping structure for table nhim_learning.cate_textbook
CREATE TABLE IF NOT EXISTS `cate_textbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_desc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `stt` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table nhim_learning.cate_textbook: 1 rows
/*!40000 ALTER TABLE `cate_textbook` DISABLE KEYS */;
REPLACE INTO `cate_textbook` (`id`, `title`, `short_desc`, `description`, `created_at`, `updated_at`, `stt`, `status`, `image`) VALUES
	(6, 'bb', 'a', '<p>ád</p>', '2021-05-13 03:48:46', '2021-05-13 08:16:11', 0, 1, 'cate_textbook/1620893771.png');
/*!40000 ALTER TABLE `cate_textbook` ENABLE KEYS */;

-- Dumping structure for table nhim_learning.celebrate
CREATE TABLE IF NOT EXISTS `celebrate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table nhim_learning.celebrate: 0 rows
/*!40000 ALTER TABLE `celebrate` DISABLE KEYS */;
/*!40000 ALTER TABLE `celebrate` ENABLE KEYS */;

-- Dumping structure for table nhim_learning.setting_home
CREATE TABLE IF NOT EXISTS `setting_home` (
  `id` int(11) NOT NULL DEFAULT 1,
  `cates_textbook` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cates_news` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci MAX_ROWS=1;

-- Dumping data for table nhim_learning.setting_home: 1 rows
/*!40000 ALTER TABLE `setting_home` DISABLE KEYS */;
REPLACE INTO `setting_home` (`id`, `cates_textbook`, `cates_news`) VALUES
	(1, '1', '2');
/*!40000 ALTER TABLE `setting_home` ENABLE KEYS */;

-- Dumping structure for table nhim_learning.textbook
CREATE TABLE IF NOT EXISTS `textbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short_desc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `isSlider` int(11) NOT NULL DEFAULT 0,
  `idCate` int(11) NOT NULL DEFAULT 0,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stt` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table nhim_learning.textbook: 0 rows
/*!40000 ALTER TABLE `textbook` DISABLE KEYS */;
/*!40000 ALTER TABLE `textbook` ENABLE KEYS */;

-- Dumping structure for table nhim_learning.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT 0,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table nhim_learning.users: 2 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `username`, `fullname`, `email`, `password`, `type`, `avatar`, `created_at`, `updated_at`, `status`) VALUES
	(78, 'a', 'a', 'asdasd23@sadasd.com', '$2y$10$CMzrFEQ4U.zFz6TQQxCigeQORM60tSbssuNRgv46OO1n1lJBmy/Rm', 0, '', '2021-05-13 07:13:31', '2021-05-13 07:13:31', 0);
REPLACE INTO `users` (`id`, `username`, `fullname`, `email`, `password`, `type`, `avatar`, `created_at`, `updated_at`, `status`) VALUES
	(68, 'abc', 'a', 'abc@gmail.com', '$2y$10$RG.cW4VznEKirxQduagbUusiShaJ7xhiC9wi0sUnD0QlqMCaqK3Zq', 1, 'avatar/1620893888.png', '2021-05-10 08:19:55', '2021-05-13 08:18:08', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
