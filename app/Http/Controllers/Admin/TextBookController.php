<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\ControllerBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\TextBook;

class TextBookController extends ControllerBase
{
    public function index () {
        return view('admin/textbook/index');
    }

    public function delete(Request $request, $id) {
        if(!$id) {
            abort(404);
        }
        if($request->isMethod('get')) {
            $token = session('token');
            $response = Http::withToken($token)->delete(config('app.url_api')."/textbook/delete/{$id}");    
            $body = $response->json();
            $message = $body['status'];
        }
        return redirect('/admin/textbooks');
    }

    public function edit($id) {
        if(!$id) {
            abort(404);
        }
        $model = TextBook::find($id);
        return view('admin/textbook/edit',['model' => $model]);
    }

    public function create() {
        return view('admin/textbook/create');
    }
}
