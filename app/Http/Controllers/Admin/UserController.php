<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\ControllerBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Users;

class UserController extends ControllerBase
{
    public function login(Request $request) {
        $params = $request->only('email', 'password');
        $message = "";
        if($request->isMethod('post') && !empty($params)) {
            $response = Http::post(config('app.url_api')."/login",
                [
                    "email" => $params['email'],
                    "password" => $params['password']
                ]
            );

            $body = $response->json();

            if($response->getStatusCode() == 200) {
                $token = $body['access_token'];
                session(['token' => $token]);

                $response = Http::withToken($token)->get(config('app.url_api').'/me');
                session(['userInfo' => $response->json()]);
                return redirect('/admin/');
            } else {
                $message = $body['error'];
            }
        }
        return view('admin/user/login', compact('message'));
    }

    public function index () {
        return view('admin/user/index');
    }

    public function logout() {
        $response = Http::withToken(session('token'))->get(config('app.url_api').'/logout');
        return redirect('/admin/user/login');
    }

    public function delete(Request $request, $id) {
        if(!$id) {
            abort(404);
        }
        if($request->isMethod('get')) {
            $userInfo = session('userInfo');
            $message = "Not correct user {$id}";
            if($userInfo != $id) {
                $token = session('token');
                $response = Http::withToken($token)->delete(config('app.url_api')."/user/delete/{$id}");    
                $body = $response->json();
                $message = $body['status'];
            }
        
        }
        return redirect('/admin/users');
    }

    public function edit($id) {
        if(!$id) {
            abort(404);
        }
        $user = Users::find($id);
        return view('admin/user/edit',['user' => $user]);
    }

    public function create() {
        return view('admin/user/create');
    }
}
