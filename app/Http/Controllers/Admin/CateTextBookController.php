<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\ControllerBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\CateTextBook;

class CateTextBookController extends ControllerBase
{
    public function index () {
        return view('admin/cate-textbook/index');
    }

    public function delete(Request $request, $id) {
        if(!$id) {
            abort(404);
        }
        if($request->isMethod('get')) {
            $token = session('token');
            $response = Http::withToken($token)->delete(config('app.url_api')."/cate-textbook/delete/{$id}");    
            $body = $response->json();
            $message = $body['status'];
        }
        return redirect('/admin/cate-textbooks');
    }

    public function edit($id) {
        if(!$id) {
            abort(404);
        }
        $model = CateTextBook::find($id);
        return view('admin/cate-textbook/edit',['model' => $model]);
    }

    public function create() {
        return view('admin/cate-textbook/create');
    }
}
