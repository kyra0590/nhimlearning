<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class ControllerBase extends Controller {
    
    public function __construct() {

        if(session('userInfo')) {
            View::share('userInfo', session('userInfo'));
        }
    }
}