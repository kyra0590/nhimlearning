<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\ControllerBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class IndexController extends ControllerBase
{
    public function index() {
        return view('admin/index/index');
    }
}
