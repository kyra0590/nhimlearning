<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Models\CateTextBook;
use Exception;

class CateTextBookController extends Controller {

    public function create(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'title' => 'required|string|max:255',
        ]);

        if($validator->fails()){
			return response()->json($validator->messages(), 400);
        }

        $status = "Create successfully";

        $model = CateTextBook::create([
            'title' => trim($request->get('title')),
            'short_desc' => $request->get('short_desc'),
            'description' => $request->get('description'),
            'stt' => $request->get('stt') ? (int) $request->get('stt') : 0,
            'status' => $request->get('status') ? (int) $request->get('status') : 0,
            'image' => $request->get('path_image') && $request->get('image') ? $request->get('path_image') .'/'. $request->get('image') : '',
        ]);

        if(!$model) {
            $status = "Create failed";
            return response()->json(compact('status'), 400);
        }
        return response()->json(compact('model','status'),201);
    }

    public function index(Request $request)
    {
         $data = ['recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => []];
        try {
            $params = $request->all();
            $limit = isset($params['length']) ? (int) $params['length'] : config('constants.items_per_page');
            $offset = isset($params['start']) ? (int) $params['start'] : 0;
            $params['title'] = isset($params['search']) ? $params['search']['value'] : '';
            if(isset($params['order'])) {
                $params['direction'] = $params['order'][0]['dir'];
                $columnID = $params['order']['0']['column'];
                $params['orderName'] = $params['columns'][$columnID]['data'];
            }
            $list = CateTextBook::getAllCateTextBook($params);
            $total = $list->count();
            $list = $list->offset($offset)->limit($limit)->get();
            if(!empty($list)) $list = $list->toArray();
            $data = [
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $list
            ];
        } catch (Exception $ex) {
            return response()->json(['status' => $ex->getMessage()], 400);
        }
        return response()->json($data);
    }

    public function edit(Request $request, $id) {
        if(!$request->isMethod('put')){
            return response()->json(['status' => 'Method is not correct'], 400);
        }
        
        $params = $request->all();

        $validator = Validator::make($params, [
			'title' => 'required|string|max:255',
        ]);

        if($validator->fails()){
			return response()->json($validator->errors()->toJson(), 400);
        }

        if(!empty($params) &&  $model = CateTextBook::find($id)) {
            $model->title = trim($params['title']);
            if(isset($params['status'])) $model->status = (int) $params['status'];
            if(isset($params['short_desc'])) $model->short_desc = trim($params['short_desc']);
            if(isset($params['description'])) $model->description = trim($params['description']);
            if(isset($params['stt'])) $model->stt = (int) $params['stt'];
            $remove_image = false;
            $oldImage = $model->image;
            if(!empty($params['image'])) {
                $remove_image = true;
                $model->image = !empty($params['path_image']) ? $params['path_image'] .'/'. $params['image'] : $params['image'];
            } else if(!empty($params['remove_image']) && $params['remove_image'] == 1) {
                $model->image = '';
                $remove_image = true;
            }
            if($remove_image && $oldImage != "" && file_exists(public_path('images').'/'.$oldImage)) {
                unlink(public_path('images').'/'.$oldImage);
            }
            if($model->save()) {
                return response()->json(['status' => "edited CateTextBook {$id} successfully"], 200);
            }
        }

        return response()->json(['status' => "edited CateTextBook {$id} failed"], 400);
    }

    public function delete(Request $request, $id) {
        if(!$request->isMethod('delete')){
            return response()->json(['status' => 'Method is not correct'], 400);
        }

        if(!$model = CateTextBook::find($id)) {
            return response()->json(['status' => "Not found CateTextBook {$id}"], 400);
        }

        if($model->delete()) {
            if($model->image != '' && file_exists(public_path('images').'/'.$model->image)){
                 unlink(public_path('images').'/'.$model->image);
            }
            return response()->json(['status' => "Delete CateTextBook {$id} is successfully"], 200);
        }

        return response()->json(['status' => "Delete CateTextBook {$id} is failed"], 400);
    }
}
