<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class IndexController extends Controller
{
    public function index(){
        die('api');
    }

    public function uploadImage(Request $request){

        $pathImage = public_path('images');

        if($request->get('path_image')) {
            $pathImage .= '/'.$request->get('path_image');
        }
        
        $postData = $request->only('image');

        if(!empty($postData['image'])) {

            $validator = Validator::make(['image' => $postData['image']], ['image' => 'mimes:jpeg,jpg,png,gif']);
        
            if ($validator->fails())
            {
                  return response()->json(['error' => $validator->errors()->getMessages()], 400);
            }

            if(!$request->image) {
                return response()->json(['status' => 'Not found images'], 400);
            }
            
            $imageName = strtotime('now').'.'.$request->image->extension();
            
            $status =  $request->image->move($pathImage, $imageName);
            
            if($status) {
                return response()->json(['path' => $request->get('path_image') ? 'images/'.$request->get('path_image') : 'images','image' => $imageName, 'status' => 'You have successfully upload image.'], 200);
            }

        } 
        return response()->json(['status' => 'Upload image failed'], 400);

    }

    public function removeImage (Request $request){
        $pathImage = $request->get('path_image');
        if($pathImage) {
            $imgSrc = public_path('images'). '/'. $pathImage;
            if($pathImage != '' && file_exists($imgSrc)) {
                unlink($imgSrc);
            }
        }

    }


}
