<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Models\Users;
use Exception;

class UserController extends Controller {

    public function login(Request $request)
    {
        // set time expired with device 
        $device = $request->get('device') ? $request->get('device') : 'pc';

        $timeConfigDevice = config('constants.time_expired');

        $timeExpired = $timeConfigDevice[$device];

        $credentials = $request->only('email', 'password');

        try {
            if (! $token = auth()->setTTL($timeExpired)->attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json($this->respondWithToken($token));
    }

    public function register(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'username' => 'required|string|max:255|unique:users',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|confirmed', // password_confirmation
            'fullname' => 'required|string|max:255',
        ]);

        if($validator->fails()){
			return response()->json($validator->messages(), 400);
        }

        $status = "Register successfully";

        $model = Users::create([
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'fullname' => $request->get('fullname'),
            'type' => $request->get('type') ? (int) $request->get('type') : 0,
            'status' => $request->get('status') ? (int) $request->get('status') : 0,
            'avatar' => $request->get('path_image') && $request->get('image') ? $request->get('path_image') .'/'. $request->get('image') : '',
        ]);

        if(!$model) {
            $status = "Register failed";
            return response()->json(compact('status'), 400);
        }
        return response()->json(compact('model','status'),201);
    }

	public function me() {
        return response()->json(auth()->user());
	}

    public function logout()
    {
        try {
            auth()->logout();
        } catch(JWTException $e) {
        	return response()->json(["status" => $e->getMessage()], 400);
        }
        return response()->json(['status' => 'Successfully logged out']);
    }

	public function refresh()
    {
        return response()->json($this->respondWithToken(auth()->refresh(true, true)));
	}

	protected function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];
    }

    public function index(Request $request)
    {
        $data = ['recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => []];
        try {
            $params = $request->all();
            $limit = isset($params['length']) ? (int) $params['length'] : config('constants.items_per_page');
            $offset = isset($params['start']) ? (int) $params['start'] : 0;
            $params['email'] = isset($params['search']) ? $params['search']['value'] : '';
            if(isset($params['order'])) {
                $params['direction'] = $params['order'][0]['dir'];
                $columnID = $params['order']['0']['column'];
                $params['orderName'] = $params['columns'][$columnID]['data'];
            }
            $list = Users::getAllUser($params);
            $total = $list->count();
            $list = $list->offset($offset)->limit($limit)->get();
            if(!empty($list)) $list = $list->toArray();
            $data = [
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $list
            ];
        } catch (Exception $ex) {
            return response()->json(['status' => $ex->getMessage()], 400);
        }
        return response()->json($data);
    }

    public function edit(Request $request, $id) {
        if(!$request->isMethod('put')){
            return response()->json(['status' => 'Method is not correct'], 400);
        }
        
        $params = $request->all();

        $validator = Validator::make($params, [
			'username' => 'required|string|max:255',
			'email' => 'string|email|max:255',
			'password' => !empty($params['password']) ? 'string|min:6' : '',
            'fullname' => 'required|string|max:255',
        ]);

        if($validator->fails()){
			return response()->json($validator->errors()->toJson(), 400);
        }

        if(!empty($params) &&  $model = Users::find($id)) {
            $model->username = trim($params['username']);
             if(isset($params['email'])) $model->email = trim($params['email']);
            if(!empty($params['password'])) $model->password = Hash::make($params['password']);
            $model->fullname = trim($params['fullname']);
            if(isset($params['status'])) $model->status = (int) $params['status'];
            if(isset($params['type'])) $model->type = (int) $params['type'];
            $remove_image = false;
            $oldImage = $model->avatar;
            if(!empty($params['image'])) {
                $remove_image = true;
                $model->avatar = !empty($params['path_image']) ? $params['path_image'] .'/'. $params['image'] : $params['image'];
            } else if(!empty($params['remove_image']) && $params['remove_image'] == 1) {
                $model->avatar = '';
                $remove_image = true;
            }
            if($remove_image && $oldImage && file_exists(public_path('images').'/'.$oldImage)) {
                unlink(public_path('images').'/'.$oldImage);
            }
            if($model->save()) {
                return response()->json(['status' => "edited account {$id} successfully"], 200);
            }
        }

        return response()->json(['status' => "edited account {$id} failed"], 400);
    }

    public function delete(Request $request, $id) {
        if(!$request->isMethod('delete')){
            return response()->json(['status' => 'Method is not correct'], 400);
        }

        if(!$model = Users::find($id)) {
            return response()->json(['status' => "Not found user {$id}"], 400);
        }

        if($model->delete()) {
            if($model->avatar != '' && file_exists(public_path('images').'/'.$model->avatar)){
                 unlink(public_path('images').'/'.$model->avatar);
            }
            return response()->json(['status' => "Delete user {$id} is successfully"], 200);
        }

        return response()->json(['status' => "Delete user {$id} is failed"], 400);
    }
}
