<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class LoginVerify extends Middleware
{
    public function handle($request, Closure $next)
    {
        $token = session('token');
        
        if($token) {
            $response = Http::withToken($token)->get(config('app.url_api')."/me");
            if($response->failed()) {
                session(['token' => '']);
            }
        }
        
        if(session('token') == "") {
            $prefix = $request->route()->getPrefix();
            if(strpos($prefix, 'admin')) {
                return redirect('/admin/user/login');
            }
           return redirect('/');
        }
        return $next($request);
    }
}
