<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Users extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'fullname',
        'username',
        'email',
        'password',
        'type',
        'status',
        'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function getAllUser($criteria){
        $mdlUser = new self;
        if(!empty($criteria)) {
            if(!empty($criteria['username'])) {
                $mdlUser = $mdlUser->where('username', 'like', '%'.$criteria['username'].'%' );
            }

            if(!empty($criteria['fullname'])) {
                $mdlUser = $mdlUser->where('fullname', 'like', '%'.$criteria['fullname'].'%' );
            }

            if(!empty($criteria['email'])) {
                $mdlUser = $mdlUser->where('email', 'like', '%'.$criteria['email'].'%' );
            }

            if(!empty($criteria['type'])) {
                $mdlUser = $mdlUser->where('type', $criteria['type']);
            }

            if(!empty($criteria['status'])) {
                $mdlUser = $mdlUser->where('status', $criteria['status']);
            }

            if(!empty($criteria['orderName'])) {
                $mdlUser = $mdlUser->orderBy($criteria['orderName'], !empty($criteria['direction']) ? $criteria['direction'] : 'asc');
            }
        }

        return $mdlUser;
    }
}
