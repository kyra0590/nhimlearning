<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextBook extends Model {

    protected $table = 'textbook';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'short_desc',
        'description',
        'isSlider',
        'idCate',
        'image',
        'status',
        'stt',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public static function getAllTextBook($criteria){
        $model = new self;
        if(!empty($criteria)) {
            if(!empty($criteria['title'])) {
                $model = $model->where('title', 'like', '%'.$criteria['title'].'%' );
            }

            if(!empty($criteria['short_desc'])) {
                $model = $model->where('short_desc', 'like', '%'.$criteria['short_desc'].'%' );
            }

            if(!empty($criteria['description'])) {
                $model = $model->where('description', 'like', '%'.$criteria['description'].'%' );
            }
            
            if(!empty($criteria['idCate'])) {
                $model = $model->where('idCate', $criteria['idCate']);
            }
            
            if(!empty($criteria['isSlider'])) {
                $model = $model->where('isSlider', $criteria['isSlider']);
            }

            if(!empty($criteria['status'])) {
                $model = $model->where('status', $criteria['status']);
            }

            if(!empty($criteria['orderName'])) {
                $model = $model->orderBy($criteria['orderName'], !empty($criteria['direction']) ? $criteria['direction'] : 'asc');
            }
        }

        return $model;
    }
}
