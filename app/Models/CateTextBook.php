<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CateTextBook extends Model {

    protected $table = 'cate_textbook';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'short_desc',
        'desciption',
        'status',
        'image',
        'stt',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public static function getAllCateTextBook($criteria){
        $model = new self;
        if(!empty($criteria)) {
            if(!empty($criteria['title'])) {
                $model = $model->where('title', 'like', '%'.$criteria['title'].'%' );
            }

            if(!empty($criteria['short_desc'])) {
                $model = $model->where('short_desc', 'like', '%'.$criteria['short_desc'].'%' );
            }

            if(!empty($criteria['description'])) {
                $model = $model->where('description', 'like', '%'.$criteria['description'].'%' );
            }

            if(!empty($criteria['status'])) {
                $model = $model->where('status', $criteria['status']);
            }

            if(!empty($criteria['orderName'])) {
                $model = $model->orderBy($criteria['orderName'], !empty($criteria['direction']) ? $criteria['direction'] : 'asc');
            }
        }

        return $model;
    }
}
