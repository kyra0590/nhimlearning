<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace(App\Http\Controllers\User::class)->group(function(){
    Route::get('/', 'IndexController@index');
});

// Custom for admin route
Route::namespace(App\Http\Controllers\Admin::class)->prefix('admin')->group(function () {

    Route::match(['post', 'get'],'/user/login', 'UserController@login');

    Route::group(['middleware' => ['login.verify']], function(){
        Route::get('/', 'IndexController@index');
        Route::get('user/logout', 'UserController@logout');
        Route::get('/users', 'UserController@index');
        Route::get('user/create', 'UserController@create');
        Route::get('user/edit/{id}', 'UserController@edit')->where('id', '[0-9]+');
        Route::get('user/delete/{id}', 'UserController@delete')->where('id', '[0-9]+');
        
        Route::get('/cate-textbooks', 'CateTextBookController@index');
        Route::get('cate-textbook/create', 'CateTextBookController@create');
        Route::get('cate-textbook/edit/{id}', 'CateTextBookController@edit')->where('id', '[0-9]+');
        Route::get('cate-textbook/delete/{id}', 'CateTextBookController@delete')->where('id', '[0-9]+');

        // Textbook
        Route::get('/textbooks', 'TextBookController@index');
        Route::get('textbook/create', 'TextBookController@create');
        Route::get('textbook/edit/{id}', 'TextBookController@edit')->where('id', '[0-9]+');
        Route::get('textbook/delete/{id}', 'TextBookController@delete')->where('id', '[0-9]+');

    });
});