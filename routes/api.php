<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::namespace(App\Http\Controllers\Api::class)->group(function(){
    Route::post('login', 'UserController@login');

    // set middleware for login
    Route::group(['middleware' => ['auth', 'cors']], function(){
        Route::get('me', 'UserController@me');
        Route::get('logout', 'UserController@logout');
        Route::get('refresh', 'UserController@refresh');
        Route::post('register', 'UserController@register');
        Route::get('users', 'UserController@index');
        Route::put('user/edit/{id}', 'UserController@edit')->where('id', '[0-9]+');
        Route::delete('user/delete/{id}', 'UserController@delete')->where('id', '[0-9]+');
        Route::post('upload/image', 'IndexController@uploadImage');
        Route::post('remove/image', 'IndexController@removeImage');
        
        Route::get('cate-textbooks', 'CateTextBookController@index');
        Route::post('cate-textbook/create', 'CateTextBookController@create');
        Route::put('cate-textbook/edit/{id}', 'CateTextBookController@edit')->where('id', '[0-9]+');
        Route::delete('cate-textbook/delete/{id}', 'CateTextBookController@delete')->where('id', '[0-9]+');

        // Textbook
        Route::get('textbooks', 'TextBookController@index');
        Route::post('textbook/create', 'TextBookController@create');
        Route::put('textbook/edit/{id}', 'TextBookController@edit')->where('id', '[0-9]+');
        Route::delete('textbook/delete/{id}', 'TextBookController@delete')->where('id', '[0-9]+');
    });
});
