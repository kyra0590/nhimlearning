// Read URL from upload file and get data:*
 function readURL(input, id, width=200, height='auto') {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#'+id).attr('src', e.target.result).width(width).height(height);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

function uploadImage(formData, handleImage){
    $.ajax({
        url: '/api/upload/image',
        type: 'POST',
        data: formData,
        headers: {
            'Authorization': "Bearer "+ token
        },
        cache: false,
        contentType: false,
        processData: false,
        async:false
    })
    .done(function(data) {
        console.log(data);
        handleImage(data);
    })
    .fail(function(data) {
        handleImage(data);
    });
}

function removeImage(pathImage) {
    $.ajax({
        url: '/api/remove/image',
        type: 'POST',
        headers: {
            'Authorization': "Bearer "+ token
        },
        data: {'path_image' : pathImage}
    });
}