@extends('admin.layout')

@section('title', 'Edit User')

@section('content')
<form id="form_edit" enctype="multipart/form-data" name='form_edit'>
    @csrf
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Type</label>
                <select class="form-control" style="width: 100%;" name='type'>
                    <?php
                        foreach (config('constants.permissions') as $key => $value) {
                            $selected = ($key === $user->type) ? 'selected' : '';
                    ?>
                        <option value="{{$key}}" {{$selected}}>{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" style="width: 100%;" name='status'>
                    <?php
                        foreach (config('constants.status') as $key => $value) {
                            $selected = ($key === $user->status) ? 'selected' : '';
                    ?>
                        <option value="{{$key}}" {{$selected}}>{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>UserName</label>
                <input type="text" class="form-control" name="username" value='{{$user->username}}'>
            </div>
             <div class="form-group">
                <label>FullName</label>
                <input type="text" class="form-control" name="fullname" value='{{$user->fullname}}'>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name='email' value='{{$user->email}}'>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name='password' value="">
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control" name='password_confirmation' value="">
            </div>
            <div class="form-group">
                <label>Avatar</label>
                <div class="input-group">
                    <a id="imgLb" href="javascript:;" data-toggle="lightbox">
                      <img width='200' id="imgInp" src="<?= ($user->avatar) ? '/public/images/'.$user->avatar : '' ?>" class="img-fluid mb-2" />
                    </a>
                </div>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="InputFile" name='image'>
                        <label class="custom-file-label" id="LabelInputFile">Choose file</label>
                        <input type="hidden" value='avatar' name='path_image'/>
                        <input type="hidden" value='' name='remove_image' id='removeImage'/>
                    </div>
                    <div class="input-group-append">
                        <a id="btn_remove" class="btn btn-block bg-gradient-danger">Remove</a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" id="btn_save" class="btn btn-block bg-gradient-info">Save</button>
                <a class="btn btn-block bg-gradient-warning" href="/admin/users">Cancel</a>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script type="text/javascript" src="{{asset('resources/js/app.js')}}"></script>
<script>
    $(document).ready(function () {
        bsCustomFileInput.init();

        $("#InputFile").change(function() {
          $("#removeImage").val("1");
          readURL(this, 'imgInp');
        });

        $("#btn_remove").bind('click', function(event) {
            event.preventDefault();
            $("#imgInp").prop('src', '').width(0).height(0);
            $("#InputFile").val("");
            $("#LabelInputFile").text('Choose file');
            $("#removeImage").val("1");
        });

        $("#form_edit").on('submit', function(event) {
            event.preventDefault();
            var formData = new FormData(this);
            var fdata = $(this).serialize();
            var imageSrc = '';
            var statusImage = true;
            uploadImage(formData, function(fImage){            
                imageSrc = fImage;
            });

            if(imageSrc) {
                if(imageSrc && imageSrc.responseJSON && imageSrc.responseJSON.errors) {
                    var errors = "upload image failed \n";
                    if(imageSrc && imageSrc.responseJSON && imageSrc.responseJSON.errors) {
                        errors += imageSrc.responseJSON.errors['image'];
                    }
                    Toast.fire({
                        icon: 'error',
                        title: errors
                    });
                    statusImage = false;
                } else if(imageSrc.image) {
                    fdata += "&image="+imageSrc.image;
               }
            }

            if(statusImage) {
                $.ajax({
                    url: '/api/user/edit/{{$user->id}}',
                    type: 'PUT',
                    dataType: 'json',
                    data: fdata,
                    headers: {
                        'Authorization': "Bearer "+token
                    },
                    async:true,
                })
                .done(function(data) {
                    if(data && data.status) {
                         Toast.fire({
                            icon: 'success',
                            title: 'edit successfully'
                          });
                        location.href = '/admin/users';
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'edit failed'
                        });

                        if(imageSrc && imageSrc.image) {
                            removeImage(imageSrc.image);
                        }
                    }
                })
                .fail(function(data) {
                    if(data && data.responseJSON) {
                        var responseJSON = data.responseJSON;
                        var errors = "";
                        for(var key  in responseJSON) {
                            errors += "* "+responseJSON[key].join("\r\n")+"\n";
                        }
                        Toast.fire({
                            icon: 'error',
                            title: 'Please check again from your inputs \n' + errors,
                        });

                    } else {
                          Toast.fire({
                            icon: 'error',
                            title: 'Error handle api server'
                        });
                    }

                    if(imageSrc && imageSrc.image) {
                        removeImage(imageSrc.image);
                    }
                });
            }
        });
    });
</script>
@endsection


