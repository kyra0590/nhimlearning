@extends('admin.layout')

@section('title', 'List User')

@section('css')
<link rel="stylesheet" href="{{ asset('resources/js/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('resources/js/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('resources/js/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('content')
<table>
  <tr>
    <td></td>
  </tr>
</table>

<table id="example2" class="table table-bordered table-hover">
    <thead>
    <tr>
    <th>Username</th>
    <th>Fullname</th>
    <th>Email</th>
    <th>Type</th>
    <th>Avatar</th>
    <th>Status</th>
    <th>Edit</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

@endsection

@section('js')
<script src="{{ asset('resources/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('resources/js/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('resources/js/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('resources/js/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $('#example2').DataTable({
      "columns": [
            { "data": "username" },
            { "data": "fullname" },
            { "data": "email" },
            { "data": "type" },
            { "data": "avatar" },
            { "data": "status" },
            { "data": "id"}
      ],
      "columnDefs": [
        {
          "targets": 6,
          "data":"id",
          "render": function ( data ) {
            return "<a class='btn btn-block bg-gradient-info' href='/admin/user/edit/"+data+"'><i class='fas fa-edit'></i></a>" +
            "<a class='btn btn-block bg-gradient-danger' href='/admin/user/delete/"+data+"'><i class='fas fa-trash-alt'></i></a>";
          }
        },
        {
          'targets': 3,
          'data':'type',
          'render' : function (data) {
             return data ? '<i class="nav-icon fas fa-user-cog"></i>' : '<i class="nav-icon far fa-user"></i>' ;
          }
        },
        {
          'targets': 4,
          'data':'avatar',
          'render' : function (data) {
            var imgLink = "/public/images/"+data;
            return data && data.length > 0  ? "<img width='100px' src='" + imgLink + "'/>" : '' ;
          }
        },
        {
          'targets': 5,
          'data':'status',
          'render' : function (data) {
            return data == 1 ? '<i class="nav-icon far fa-eye"></i>' : data == 0 ? '<i class="nav-icon far fa-eye-slash"></i>' : '<i class="nav-icon fas fa-trash"></i>' ;
          }
        }
      ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "pageLength": "{{ config('constants.items_per_page')}}",
      'ajax': {
        'url' : "{{ config('app.url_api').'/users' }}",
        'type' : 'get',
        'headers': {
          'Authorization': "Bearer " + token
        },
      },
      "dom": '<"toolbar">frtip'
    });

    $("div.toolbar").html('<a href="/admin/user/create" class="btn btn-block bg-gradient-info" style="width:100px">Add</a>');
  });
</script>
@endsection