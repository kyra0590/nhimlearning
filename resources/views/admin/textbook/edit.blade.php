@extends('admin.layout')

@section('title', 'Edit Category TextBook')

@section('content')
<form id="form_edit" enctype="multipart/form-data" name='form_edit'>
    @csrf
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" style="width: 100%;" name='status'>
                    <?php
                        foreach (config('constants.status_items') as $key => $value) {
                            $selected = ($key === $model->status) ? 'selected' : '';
                    ?>
                        <option value="{{$key}}" {{$selected}}>{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" name="title" value='{{$model->title}}'>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Category TextBook</label>
                <select class="form-control" style="width: 100%;" name='idCate' id='idCate'>
                <option value="0">Please select</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Is Slider</label>
                <select class="form-control" style="width: 100%;" name='isSlider'>
                    <?php
                        foreach (config('constants.status_items') as $key => $value) {
                            $selected = ($key === $model->isSlider) ? 'selected' : '';
                    ?>
                         <option value="{{$key}}" {{$selected}}>{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
         <div class="col-lg-10">
             <div class="form-group">
                <label>Short Descriptions</label>
                <textarea class="form-control" rows="5" placeholder="Enter ..." name='short_desc'>{{$model->short_desc}}</textarea>
            </div>
            <div class="form-group">
                <label>Descriptions</label>
                <textarea class="form-control editor" name="description">{{$model->description}}</textarea>
            </div>
        </div>
         <div class="col-lg-6">
            <div class="form-group">
                <label>Image</label>
                <div class="input-group">
                    <a id="imgLb" href="javascript:;" data-toggle="lightbox">
                        <img width='200' id="imgInp" src="<?= ($model->image) ? '/public/images/'.$model->image : '' ?>" class="img-fluid mb-2" />
                    </a>
                </div>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="InputFile" name='image'>
                        <label class="custom-file-label" id="LabelInputFile">Choose file</label>
                        <input type="hidden" value='textbook' name='path_image'/>
                        <input type="hidden" value='0' name='remove_image' id='removeImage'/>
                    </div>
                    <div class="input-group-append">
                        <a id="btn_remove" class="btn btn-block bg-gradient-danger">Remove</a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" id="btn_save" class="btn btn-block bg-gradient-info">Save</button>
                <a class="btn btn-block bg-gradient-warning" href="/admin/textbooks">Cancel</a>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script type="text/javascript" src="{{asset('resources/js/app.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.editor').summernote({ height: 300 });
        
        bsCustomFileInput.init();

        $("#InputFile").change(function() {
          $("#removeImage").val("0");
          readURL(this, 'imgInp');
        });

        $("#btn_remove").bind('click', function(event) {
            event.preventDefault();
            $("#imgInp").prop('src', '').width(0).height(0);
            $("#InputFile").val("");
            $("#LabelInputFile").text('Choose file');
            $("#removeImage").val("1");
        });

       $("#form_edit").on('submit', function(event) {
            event.preventDefault();
            var formData = new FormData(this);
            var fdata = $(this).serialize();
            var imageSrc = '';
            var statusImage = true;
            uploadImage(formData, function(fImage){            
                imageSrc = fImage;
            });

            if(imageSrc) {
                if(imageSrc && imageSrc.responseJSON && imageSrc.responseJSON.errors) {
                    var errors = "upload image failed \n";
                    if(imageSrc && imageSrc.responseJSON && imageSrc.responseJSON.errors) {
                        errors += imageSrc.responseJSON.errors['image'];
                    }
                    Toast.fire({
                        icon: 'error',
                        title: errors
                    });
                    statusImage = false;
                } else if(imageSrc.image) {
                    fdata += "&image="+imageSrc.image;
               }
            }

            if(statusImage) {
                $.ajax({
                    url: "{{ config('app.url_api').'/textbook/edit/'.$model->id }}",
                    type: 'PUT',
                    dataType: 'json',
                    data: fdata,
                    headers: {
                        'Authorization': "Bearer "+token
                    },
                    async:true,
                })
                .done(function(data) {
                    if(data && data.status) {
                         Toast.fire({
                            icon: 'success',
                            title: 'edit successfully'
                          });

                        location.href = '/admin/textbooks';
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'edit failed'
                        });

                        if(imageSrc && imageSrc.image) {
                            removeImage(imageSrc.image);
                        }
                    }
                })
                .fail(function(data) {
                    if(data && data.responseJSON) {
                        var responseJSON = data.responseJSON;
                        var errors = "";
                        for(var key  in responseJSON) {
                            errors += "* "+responseJSON[key].join("\r\n")+"\n";
                        }
                        Toast.fire({
                            icon: 'error',
                            title: 'Please check again from your inputs \n' + errors,
                        });

                    } else {
                          Toast.fire({
                            icon: 'error',
                            title: 'Error handle api server'
                        });
                    }

                    if(imageSrc && imageSrc.image) {
                        removeImage(imageSrc.image);
                    }
                });
            }
        });

        const loadCateTextBook = (idSelected) => {
            $.ajax({
                url: "{{ config('app.url_api').'/cate-textbooks' }}",
                type: 'GET',
                dataType: 'json',
                data: {"order":[{"dir":"asc", "column":'0',"status":1}], "columns":[{"data":"stt"}]},
                headers: {
                    'Authorization': "Bearer "+token
                },
                async:true,
            })
            .done(function(result) {
                if(result && result.data && result.data.length > 0) {
                    data = result.data;
                    data.map((item) => {
                        return $("#idCate").append(new Option(item.title,item.id));
                    });
                    if(idSelected !== '') {
                        $("#idCate").val(idSelected);
                    }
                }
            });
        };
        var idSelectedCate = "{{$model->idCate}}";
        loadCateTextBook(idSelectedCate);
    });
</script>
@endsection


