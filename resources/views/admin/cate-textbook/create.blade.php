@extends('admin.layout')

@section('title', 'Create Category TextBook')

@section('content')
<form id="form_edit" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" style="width: 100%;" name='status'>
                    <?php
                        foreach (config('constants.status_items') as $key => $value) {
                    ?>
                        <option value="{{$key}}">{{$value}}</option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" name="title" value=''>
            </div>
        </div>
         <div class="col-lg-10">
             <div class="form-group">
                <label>Short Descriptions</label>
                <textarea class="form-control" rows="5" placeholder="Enter ..." name='short_desc'></textarea>
            </div>
            <div class="form-group">
                <label>Descriptions</label>
                <textarea class="form-control editor" name="description"></textarea>
            </div>
        </div>
         <div class="col-lg-6">
            <div class="form-group">
                <label>Image</label>
                <div class="input-group">
                    <a id="imgLb" href="javascript:;" data-toggle="lightbox">
                      <img id="imgInp" src="" class="img-fluid mb-2" />
                    </a>
                </div>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="InputFile" name='image'>
                        <label class="custom-file-label" id="LabelInputFile">Choose file</label>
                        <input type="hidden" value='cate_textbook' name='path_image'>
                    </div>
                    <div class="input-group-append">
                        <a id="btn_remove" class="btn btn-block bg-gradient-danger">Remove</a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" id="btn_save" class="btn btn-block bg-gradient-info">Save</button>
                <a class="btn btn-block bg-gradient-warning" href="/admin/cate-textbooks">Cancel</a>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script type="text/javascript" src="{{asset('resources/js/app.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.editor').summernote({ height: 300 });

        bsCustomFileInput.init();

        $("#InputFile").change(function() {
        readURL(this, 'imgInp');
        });

        $("#btn_remove").bind('click', function(event) {
            event.preventDefault();
            $("#imgInp").prop('src', '').width(0).height(0);
            $("#InputFile").val("");
            $("#LabelInputFile").text('Choose file');
        });

        $("#form_edit").on('submit', function(event) {
            event.preventDefault();
            var formData = new FormData(this);
            var fdata = $(this).serialize();
            var imageSrc = '';
            var statusImage = true;
            uploadImage(formData, function(fImage){
                imageSrc = fImage;
            });

            if(imageSrc) {
                if(imageSrc && imageSrc.responseJSON && imageSrc.responseJSON.errors) {
                    var errors = "upload image failed \n";
                    if(imageSrc && imageSrc.responseJSON && imageSrc.responseJSON.errors) {
                        errors += imageSrc.responseJSON.errors['image'];
                    }
                    Toast.fire({
                        icon: 'error',
                        title: errors
                    });
                    statusImage = false;
                } else if(imageSrc.image) {
                    fdata += "&image="+imageSrc.image;
            }
            }

            if(statusImage) {
                $.ajax({
                    url: '/api/cate-textbook/create',
                    type: 'POST',
                    dataType: 'json',
                    data: fdata,
                    headers: {
                        'Authorization': "Bearer "+token
                    },
                    async:true,
                })
                .done(function(data) {
                    if(data && data.status) {
                        Toast.fire({
                            icon: 'success',
                            title: 'create successfully'
                        });
                        location.href = '/admin/cate-textbooks';
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'create failed'
                        });

                        if(imageSrc && imageSrc.image) {
                            removeImage(imageSrc.image);
                        }
                    }
                })
                .fail(function(data) {
                    if(data && data.responseJSON) {
                        var responseJSON = data.responseJSON;
                        var errors = "";
                        for(var key  in responseJSON) {
                            errors += "* "+responseJSON[key].join("\r\n")+"\n";
                        }
                        Toast.fire({
                            icon: 'error',
                            title: 'Please check again from your inputs \n' + errors,
                        });

                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'Error handle api server'
                        });
                    }

                    if(imageSrc && imageSrc.image) {
                        removeImage(imageSrc.image);
                    }
                });
            }
        });
    });
</script>
@endsection


