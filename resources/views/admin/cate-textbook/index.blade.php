@extends('admin.layout')

@section('title', 'List Category TextBook')

@section('css')
<link rel="stylesheet" href="{{ asset('resources/js/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('resources/js/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('resources/js/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endsection

@section('content')
<table>
  <tr>
    <td></td>
  </tr>
</table>

<table id="example2" class="table table-bordered table-hover">
    <thead>
    <tr>
    <th>ID</th>
    <th>Title</th>
    <th>Short Descriptions</th>
    <th>Image</th>
    <th>Status</th>
    <th>Edit</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

@endsection

@section('js')
<script src="{{ asset('resources/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('resources/js/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('resources/js/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('resources/js/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
  $(function () {
    $('#example2').DataTable({
      "columns": [
            { "data": "id" },
            { "data": "title" },
            { "data": "short_desc" },
            { "data": "image" },
            { "data": "status" },
            { "data": "id"}
      ],
      "columnDefs": [
        {
          "targets": 5,
          "data":"id",
          "render": function ( data ) {
            return "<a class='btn btn-block bg-gradient-info' href='/admin/cate-textbook/edit/"+data+"'><i class='fas fa-edit'></i></a>" +
            "<a class='btn btn-block bg-gradient-danger' href='/admin/cate-textbook/delete/"+data+"'><i class='fas fa-trash-alt'></i></a>";
          }
        },
        {
          'targets': 3,
          'data':'image',
          'render' : function (data) {
            var imgLink = "/public/images/"+data;
            return data && data.length > 0  ? "<img width='100px' src='" + imgLink + "'/>" : '' ;
          }
        },
        {
          'targets': 4,
          'data':'status',
          'render' : function (data) {
            return data == 1 ? '<i class="nav-icon far fa-eye"></i>' : data == 0 ? '<i class="nav-icon far fa-eye-slash"></i>' : '<i class="nav-icon fas fa-trash"></i>' ;
          }
        }
      ],
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "pageLength": "{{ config('constants.items_per_page')}}",
      'ajax': {
        'url' : "{{ config('app.url_api').'/cate-textbooks' }}",
        'type' : 'get',
        'headers': {
          'Authorization': "Bearer {{ session('token') }}"
        },
      },
      "dom": '<"toolbar">frtip'
    });

    $("div.toolbar").html('<a href="/admin/cate-textbook/create" class="btn btn-block bg-gradient-info" style="width:100px">Add</a>');
  });
</script>
@endsection