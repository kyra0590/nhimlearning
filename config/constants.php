<?php 
    return  [
        'permissions' => [ 'user', 'admin' ],
        'status' => ['inactived', 'actived', 'deleted'],
        'time_expired' => [
        	'pc' => env('TIME_EXPIRED_PC', 10080), // 7 days
        	'phone' => env('TIME_EXPIRED_PHONE', 525600), // 365 days
        ],
        'items_per_page' => env('ITEMS_PERS_PAGE', 30),
        'status_items' => ['hide', 'show'],
    ];

